import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    private UserService userService;

    @Mock
    UserRole userRole;

    @BeforeEach
    public void setUp(){
        userService = new UserService();
    }



    //Given
    @ParameterizedTest
    @CsvSource(value = {"anna , losen" , "berit , 123456" , "kalle , password"})
    public void userLogin(String userName , String password){


        //When
        boolean loginSuccess = userService.userLogin(userName, password);

        //Then
        assertTrue(loginSuccess);
    }

    //Given
    @ParameterizedTest
    @CsvSource(value = {"anna , losen , YW5uYQ==" , "berit , 123456 , YmVyaXQ=", "kalle , password , a2FsbGU="})
    public void base64Code(String username, String password, String expectedToken)  {

        //When
        String loginToken = userService.getToken(username , password);

        //Then
        assertEquals(expectedToken, loginToken);
        //When ,Then

    }



    @Test
    public void base64CodeShouldThrowException(){
        String username = "önna";
        String password = "lesen";
        //When ,Then
        UserNotFoundException err = assertThrows(UserNotFoundException.class, () -> userService.getToken(username, password));
        assertEquals("User not found!" , err.getMessage());
    }



    @Test
    public void validateToken(){
        //Given
        String token = "YW5uYQ==";
        //When
        boolean tokenValidated = userService.validateToken(token);

        //Then
        assertTrue(tokenValidated);
    }

    @ParameterizedTest
    @CsvSource(value = {
            "anna , eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhbm5hIiwiUm9sZSI6IkFETUlOIn0.ilMEI7YPIjtezU2tmpIl0PYPPueiRnYw8G34K0dmsRI ",
            "berit , eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJiZXJpdCIsIlJvbGUiOiJBRE1JTiJ9.jzmsRGLTd4tVgszMy3z4MLsMs2yMHseBtZMLvUnJDCw",
            "kalle , eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJrYWxsZSIsIlJvbGUiOiJBRE1JTiJ9.5fIkK-CKDsAKsORWiv0MqX1eLOAtYgNYc4roKqQrt5U"})
    public void JWTTokenValidate(String username, String expectedJWTToken){

        UserJwt userJwt = new UserJwt(userRole);

        //When
        String jwtToken = userJwt.getJWTToken(username);

        //Then
        assertEquals(jwtToken, expectedJWTToken);

    }

    @ParameterizedTest
    @CsvSource(value = {"anna , STUDENT", "berit , TEACHER" , "kalle , ADMIN"})
    public void getUserRole(String username, String actualRole){

        when(userRole.getUserRole(username)).thenReturn(actualRole);

        UserJwt userJwt = new UserJwt(userRole);

        //When
        String expectedRole = userJwt.getUserRole(username);

        //Then
        assertEquals(expectedRole , actualRole);

    }
}
