import java.util.Arrays;
import java.util.Base64;
import java.util.List;

public class UserService {

    List<User> userList = Arrays.asList(
            new User("anna" , "losen"),
            new User("berit" , "123456"),
            new User("kalle" , "password"));



    public boolean userLogin(String userName, String password) {

       for (User user : userList){
           if (userName.equals(user.getUsername()) && password.equals(user.getPassword())){
               return true;
           }
       }
        return false;
    }

    public String getToken(String username, String password)  {
        for (User user : userList) {
            if (username.equals(user.getUsername())  && password.equals(user.getPassword())) {
                byte[] originalAsBytes = username.getBytes();
                byte[] base64bytes = Base64.getEncoder().encode(originalAsBytes);
                String base64String = new String(base64bytes);

                return base64String;
            }
        }
        throw new UserNotFoundException("User not found!");


    }


    public boolean validateToken(String token) {


            byte[]backAsBase64Bytes = token.getBytes();
            byte[]backAsBytes = Base64.getDecoder().decode(backAsBase64Bytes);
            String backAsString = new String(backAsBytes);

            for (User user: userList) {
                if (user.getUsername().equals(backAsString)){
                    return true;
                }
            }
            return false;

    }
}
