import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import java.security.Key;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class UserJwt {

    List<User> userList = Arrays.asList(
            new User("anna" , "losen"),
            new User("berit" , "123456"),
            new User("kalle" , "password"));

    private UserRole userRole;

    public UserJwt(UserRole userRole) {
        this.userRole = userRole;
    }

    public String getJWTToken(String username) {
        Key key = Keys.hmacShaKeyFor("ThisIsMyVerySecretKeyThatNooneWillEverFound".getBytes());

        String userRoleKey = userRole.getUserRole(username);

         return Jwts.builder()
                        .setSubject(username)
                        .addClaims(Map.of("Role" , userRoleKey))
                        .signWith(key)
                        .compact();

    }

    public String getUserRole(String username) {
        String usersRole = null;
        for (User user : userList){
            if (user.getUsername().equals(username)){
                usersRole = userRole.getUserRole(username);
            }
        }
        return usersRole;
    }

}
